# MERN QR Application

## First Step
* `cd server/ && npm i`
* `cd client/ && npm i`

## Second Step
* Run MongoDB with the command `mongod`
* Inside `server/` folder run `node index.js`
* Inside `client/` folder run `npm start`

