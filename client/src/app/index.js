import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import { NavBar } from '../components'
import { AppList, AppInsert, AppUpdate } from '../pages'

import 'bootstrap/dist/css/bootstrap.min.css'

function App() {
    return (
        <div>
            <Router>
                <NavBar />
                <Switch>
                    <Route path="/application/list" exact component={AppList} />
                    <Route path="/application/create" exact component={AppInsert} />
                    <Route
                        path="/application/update/:id"
                        exact
                        component={AppUpdate}
                    />
                </Switch>
            </Router>
            <AppList />
        </div>
    )
}

export default App
