import React, { Component } from 'react'
import ReactTable from 'react-table'
import api from '../api'

import styled from 'styled-components'

import 'react-table/react-table.css'

import { Dropdown } from 'react-bootstrap'

const Wrapper = styled.div`
    padding: 0 40px 40px 40px;
`

const Update = styled.div`
    color: #0070c9;
    cursor: pointer;
    padding-top: 1vh;
`

const Insert = styled.div`
    color: #ef9b0f;
    cursor: pointer;
    padding-top: 1vh;
`

const Delete = styled.div`
    color: #ff0000;
    cursor: pointer;
    padding-top: 1vh;
    padding-left: 5vh;
`

// const ShowQRButton = styled.button.attrs({
//     className: `btn btn-link`,
// })`
//     padding-top: 10px;
// `

const List = styled.div.attrs({
    className: 'navbar-nav mr-auto',
})``

const Item = styled.div.attrs({
    className: 'collpase navbar-collapse',
})`
    color: #ef9b0f;
    cursor: pointer;
    padding-left: 5px;
    padding-top: 10px;
`

class InsertApp extends Component {
    insertNewApp = event => {
        event.preventDefault()
        window.location.href = `/application/create`
    }

    render() {
        return <Insert onClick={this.insertNewApp}>Add new</Insert>
    }
}

class UpdateApp extends Component {
    updateUser = event => {
        event.preventDefault()
        window.location.href = `/application/update/${this.props.id}`
    }

    render() {
        return <Update onClick={this.updateUser}>{this.props.name}</Update>
    }
}

class DeleteApp extends Component {
    deleteUser = event => {
        event.preventDefault()
        this.deleteEvent(this.props.id)
    }
    deleteEvent = async (id) => {
        await api.deleteApplicationById(id).then(res => {
            this.forceUpdate()
            window.location.reload()
        })
    }

    render() {
        return <Delete onClick={this.deleteUser}>X</Delete>
    }
}

// class ShowQR extends Component {
//     showQRfunction = () => {
//         var img = new Image()
//         img.src = this.props.image

//         let qr = window.open('')
//         qr.document.write(img.outerHTML)
//     }
//     render() {
//         return <ShowQRButton onClick={() => this.showQRfunction()}>Show image</ShowQRButton>
//     }
// }

class AppsList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            applications: [],
            columns: [],
            isLoading: false,
            insert: false
        }
    }

    componentDidMount = async () => {
        this.setState({ isLoading: true })

        await api.getAllApplications().then(applications => {
            this.setState({
                applications: applications.data.data,
                isLoading: false,
            })
        })
    }

    handleInsertNewApplication = () => {
        this.setState({ insert: true })
    }

    render() {
        const { applications, isLoading } = this.state

        const columns = [
            {
                Header: 'Name',
                accessor: 'name',
                // filterable: true,
                Cell: function(props) {
                    return (
                        <span>
                            <UpdateApp id={props.original._id} name={props.original.name} />
                        </span>
                    )
                },
                width: 300
            },
            {
                Header: '',
                accessor: '',
                Cell: function(props) {
                    return (
                        <span>
                            <DeleteApp id={props.original._id} />
                        </span>
                    )
                },
                width: 100
            }
        ]

        return (
            <div
             style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', width: '100%' }}
            >
                <Wrapper>
                    <Dropdown>
                        <Dropdown.Toggle
                         style={{ width: '50vh' }}
                         variant='success'
                         id='dropdown-basic'
                        >
                            Projects
                        </Dropdown.Toggle>
                            <Dropdown.Menu>
                                <ReactTable
                                    style={{ width: '50vh' }}
                                    data={applications}
                                    columns={columns}
                                    loading={isLoading}
                                    // defaultPageSize={10}
                                    showPageSizeOptions={false}
                                    showPagination={false}
                                    minRows={0}
                                />
                                <List>
                                    {/* <Item onClick={() => this.handleInsertNewApplication()}>
                                        Add New
                                    </Item> */}
                                    <Item>
                                        <span>
                                            <InsertApp />
                                        </span>
                                    </Item>
                                </List>
                            </Dropdown.Menu>
                    </Dropdown>
                </Wrapper>
            </div>
        )
    }
}

export default AppsList
