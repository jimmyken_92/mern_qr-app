import React, { Component } from 'react'
import api from '../api'

import styled from 'styled-components'
import { QRCode } from 'react-qr-svg'
import htmlToImage from 'html-to-image'
import download from 'downloadjs'

// const Title = styled.h1.attrs({
//     className: 'h1',
// })``

const Wrapper = styled.div.attrs({
    className: 'form-group',
})`
    margin: 0 30px;
`

const Label = styled.label`
    margin: 5px;
`

const InputText = styled.input.attrs({
    className: 'form-control',
})`
    margin: 5px;
`

const Button = styled.button.attrs({
    className: `btn btn-primary`,
})`
    margin: 15px 15px 15px 5px;
`

const CancelButton = styled.a.attrs({
    className: `btn btn-danger`,
})`
    margin: 15px 15px 15px 5px;
`

// const DownloadButton = styled.button.attrs({
//     className: `btn btn-outline-info`,
// })`
//     margin: 13px 13px 13px 3px;
// `

class AppInsert extends Component {
    constructor(props) {
        super(props)

        this.state = {
            name: '',
            instagramid: '',
            url: '',
            setqr: false
        }
    }

    handleChangeInputName = async event => {
        const name = event.target.value
        this.setState({ name })

        // generate qr
        this.setState({ setqr: true })
    }
    handleChangeInputIG = async event => {
        const instagramid = event.target.value
        this.setState({ instagramid })

        // generate qr
        this.setState({ setqr: true })
    }
    handleChangeInputURL = async event => {
        const url = event.target.value
        this.setState({ url })

        // generate qr
        this.setState({ setqr: true })
    }
    // downloadGeneratedQR = () => {
    //     const { name } = this.state
    //     htmlToImage.toPng(document.getElementById('generated-qr'))
    //         .then(function(image) {
    //             download(image, 'qr_'+name+'.png')
    //         })
    // }
    handleIncludeData = () => {
        const { name } = this.state

        htmlToImage.toPng(document.getElementById('generated-qr'))
            .then((res) => {
                return res
            })
            .then((image) => {
                download(image, 'qr_'+name+'.png')
                this.handleIncludeApp()
                // if (
                //     window.confirm(
                //         download(image, 'qr_'+name+'.png'),
                //     )
                // ) {
                //     this.setState({ image })
                //     this.handleIncludeApp()
                // }
            })
    }

    handleIncludeApp = async () => {
        const { name, instagramid, url } = this.state
        const payload = { name, instagramid, url }

        await api.insertApplication(payload).then(res => {
            window.alert(`Inserted successfully`)
            this.setState({
                name: '',
                instagramid: '',
                url: '',
                setqr: false
            })
            this.forceUpdate()
            window.location.href = '/'
        })
    }

    render() {
        const { name, instagramid, url } = this.state
        return (
            <div
             style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', width: '100%' }}
            >
                <Wrapper
                style={{ width: '50vh' }}
                >
                    {/* <Title>Create New QR Application</Title> */}

                    <Label>Name: </Label>
                    <InputText
                        type="text"
                        value={name}
                        onChange={this.handleChangeInputName}
                    />

                <Label>Instagram ID: </Label>
                    <InputText
                        type="text"
                        value={instagramid}
                        onChange={this.handleChangeInputIG}
                    />

                    <Label>URL: </Label>
                    <InputText
                        type="text"
                        value={url}
                        onChange={this.handleChangeInputURL}
                    />

                    <Label>QR: </Label>
                    <br />
                    <small>QR generates automatically</small>
                    <br />
                    {
                        this.state.setqr ?
                            (
                                <Wrapper>
                                    <QRCode
                                        id='generated-qr'
                                        bgColor='#FFFFFF'
                                        fgColor='#000000'
                                        level='Q'
                                        style={{ width: 256 }}
                                        value={url+','+instagramid}
                                    /><br />
                                    {/* <DownloadButton onClick={() => this.downloadGeneratedQR()}>Download image</DownloadButton> */}
                                </Wrapper>
                            )
                        :
                            (null)
                    }
                    <br />

                    <Button onClick={this.handleIncludeData}>Add/Download</Button>
                    <CancelButton href={'/'}>Cancel</CancelButton>
                </Wrapper>
            </div>
        )
    }
}

export default AppInsert
