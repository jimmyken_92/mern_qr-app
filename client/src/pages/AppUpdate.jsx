import React, { Component } from 'react'
import api from '../api'

import styled from 'styled-components'
import { QRCode } from 'react-qr-svg'
import htmlToImage from 'html-to-image'
import download from 'downloadjs'

// const Title = styled.h1.attrs({
//     className: 'h1',
// })``

const Wrapper = styled.div.attrs({
    className: 'form-group',
})`
    margin: 0 30px;
`

const Label = styled.label`
    margin: 5px;
`

const InputText = styled.input.attrs({
    className: 'form-control',
})`
    margin: 5px;
`

const Button = styled.button.attrs({
    className: `btn btn-primary`,
})`
    margin: 15px 15px 15px 5px;
`

const CancelButton = styled.a.attrs({
    className: `btn btn-danger`,
})`
    margin: 15px 15px 15px 5px;
`

// const DownloadButton = styled.button.attrs({
//     className: `btn btn-outline-info`,
// })`
//     margin: 13px 13px 13px 3px;
// `

class AppsUpdate extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            name: '',
            instagramid: '',
            url: '',
            setqr: false
        }
    }

    handleChangeInputName = async event => {
        const name = event.target.value
        this.setState({ name })

        // re-generate qr
        this.setState({ setqr: true })
    }
    handleChangeInputIG = async event => {
        const instagramid = event.target.value
        this.setState({ instagramid })

        // re-generate qr
        this.setState({ setqr: true })
    }
    handleChangeInputURL= async event => {
        const url = event.target.validity.valid
            ? event.target.value
            : this.state.url

        this.setState({ url })

        // re-generate qr
        this.setState({ setqr: true })
    }
    handleChangeData = () => {
        const { name } = this.state

        htmlToImage.toPng(document.getElementById('qr-image'))
            .then((res) => {
                return res
            })
            .then((qr) => {
                download(qr, 'updated_'+name+'.png')
                this.setState({ image: qr })
                this.handleUpdateApp()
            })
    }
    // downloadGeneratedQR = () => {
    //     const { name } = this.state
    //     htmlToImage.toPng(document.getElementById('qr-image'))
    //         .then(function(image) {
    //             download(image, 'updated_'+name+'.png')
    //         })
    // }

    handleUpdateApp = async () => {
        const { id, name, instagramid, url } = this.state
        const payload = { name, instagramid, url }

        await api.updateApplicationById(id, payload).then(res => {
            window.alert(`Application updated successfully`)
            this.setState({
                name: '',
                instagramid: '',
                url: ''
            })
            this.forceUpdate()
            window.location.href = '/'
        })
    }

    componentDidMount = async () => {
        const { id } = this.state
        const app = await api.getApplicationById(id)

        this.setState({
            name: app.data.data.name,
            instagramid: app.data.data.instagramid,
            url: app.data.data.url
        })
    }

    render() {
        const { name, instagramid, url } = this.state
        return (
            <div
             style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', width: '100%' }}
            >
                <Wrapper
                style={{ width: '50vh' }}
                >
                    {/* <Title>Create Application</Title> */}

                    <Label>Name: </Label>
                    <InputText
                        type="text"
                        value={name}
                        onChange={this.handleChangeInputName}
                    />

                    <Label>Instagram ID: </Label>
                    <InputText
                        type="text"
                        value={instagramid}
                        onChange={this.handleChangeInputIG}
                    />

                    <Label>URL: </Label>
                    <InputText
                        type="text"
                        value={url}
                        onChange={this.handleChangeInputURL}
                    />

                    <Wrapper>
                        <QRCode
                            id='qr-image'
                            bgColor='#FFFFFF'
                            fgColor='#000000'
                            level='Q'
                            style={{ width: 256 }}
                            value={this.state.url+','+this.state.instagramid}
                        /><br />
                        {/* <DownloadButton onClick={() => this.downloadGeneratedQR()}>Download image</DownloadButton> */}
                    </Wrapper>
                    <br />

                    <Button onClick={this.handleChangeData}>Update Application</Button>
                    <CancelButton href={'/'}>Cancel</CancelButton>
                </Wrapper>
            </div>
        )
    }
}

export default AppsUpdate
