import axios from 'axios'

const api = axios.create({
    baseURL: 'http://localhost:8081/api'
})

export const insertApplication = payload => api.post(`/app`, payload)
export const getAllApplications = () => api.get(`/applist`)
export const updateApplicationById = (id, payload) => api.put(`/app/${id}`, payload)
export const deleteApplicationById = id => api.delete(`/app/${id}`)
export const getApplicationById = id => api.get(`/app/${id}`)

const apis = {
    insertApplication,
    getAllApplications,
    updateApplicationById,
    deleteApplicationById,
    getApplicationById,
}

export default apis
