const App = require('../models/app-model')
// const multer = require('multer')

// const Storage = multer.diskStorage({
//     destination(req, file, callback) {
//         callback(null, './../public/images')
//     }
// })
// const upload = multer({ storage: Storage })

createApplication = (req, res) => {
    const body = req.body
    // console.log(body)

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide an Application',
        })
    }

    const app = new App(body)

    if (!app) {
        return res.status(400).json({ success: false, error: err })
    }

    app
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id: app._id,
                message: 'New Application created!',
            })
        })
        .catch(error => {
            return res.status(400).json({
                error,
                message: 'Application not created! Check provided data',
            })
        })
}

updateApplication = async (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    }

    App.findOne({ _id: req.params.id }, (err, app) => {
        if (err) {
            return res.status(404).json({
                err,
                message: 'Application not found!',
            })
        }
        app.name = body.name
        app.url = body.url
        app.instagramid = body.instagramid

        app
            .save()
            .then(() => {
                return res.status(200).json({
                    success: true,
                    id: app._id,
                    message: 'Application updated!',
                })
            })
            .catch(error => {
                return res.status(404).json({
                    error,
                    message: 'Application not updated!',
                })
            })
    })
}

deleteApplication = async (req, res) => {
    await App.findOneAndDelete({ _id: req.params.id }, (err, app) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!app) {
            return res
                .status(404)
                .json({ success: false, error: `Application not found` })
        }
        // console.log('success')
        return res.status(200).json({ success: true, data: app })
    }).catch(err => console.log(err))
}

getApplicationById = async (req, res) => {
    await App.findOne({ _id: req.params.id }, (err, app) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        return res.status(200).json({ success: true, data: app })
    }).catch(err => console.log(err))
}

getApplications = async (req, res) => {
    await App.find({}, (err, app) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!app.length) {
            return res
                .status(404)
                .json({ success: false, error: `Application not found` })
        }
        // console.log(app)
        return res.status(200).json({ success: true, data: app })
    }).catch(err => console.log(err))
}

module.exports = {
    createApplication,
    updateApplication,
    deleteApplication,
    getApplications,
    getApplicationById,
}
