const express = require('express')

const AppCtrl = require('../controllers/app-ctrl')

const router = express.Router()

router.post('/app', AppCtrl.createApplication)
router.put('/app/:id', AppCtrl.updateApplication)
router.delete('/app/:id', AppCtrl.deleteApplication)
router.get('/app/:id', AppCtrl.getApplicationById)
router.get('/applist', AppCtrl.getApplications)

module.exports = router
